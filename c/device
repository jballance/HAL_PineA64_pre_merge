/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * cddl/RiscOS/Sources/HWSupport/SD/SDIODriver/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2012 Ben Avison.  All rights reserved.
 * Portions Copyright 2019 Michael Grunditz
 * Portions Copyright 2019 John Ballance
 * Portions Copyright 2021 RISC OS Developments Ltd
 * Use is subject to license terms.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "Interface/SDHCIDevice.h"

#include "SMHCLib/SMHCLib.h"
#include "SyncLib/barrier.h"

#include "pinea64.h"
#include "register.h"

/* A quick macro to silence compiler warnings about unused parameters */
#define IGNORE(x) do { (void)(x); } while(0)

/* Interrupt device numbers */
#define SMHC0_IRQ 92+32
#define SMHC1_IRQ 93+32
#define SMHC2_IRQ 94+32

/* Timeout waiting for CMD_LOAD bit to clear in SD_CMD register, in centiseconds */
#define CMD_LOAD_TIMEOUT 50

typedef struct
{
  smhc_controller_t smhc;
  volatile void *ccu;               // Logical address of CCU (clock control unit)
  volatile void *pc;                // Logical address of CPUx port controller
  volatile void *pc2;               // Logical address of CPUs port controller
  uint8_t       clk_reg;            // Address of SDMMCx_CLK_REG within CCU
  uint8_t       soft_rst_bit;       // Bit within BUS_SOFT_RST_REG0
  uint8_t       port;               // GPIO port configuring our pins
  uint8_t       cmd_pin;            // Pin within GPIO port corresponding to CMD signal
} controller_t;

void microdelay(uint32_t us, void *HALsb);
unsigned int readcounter(void *HALsb);
uint32_t callRSBReadByte(uint8_t ignored, uint8_t reg, void *HALsb);
void callRSBWriteByte(uint8_t value, uint8_t reg, void *HALsb);

static inline void setreg(volatile void *base, uint32_t reg, uint32_t val)
{
  barrier_sync();
  *(volatile uint32_t *)((uintptr_t) base + reg) = val;
}

static inline uint32_t rreg(volatile void *base, uint32_t reg)
{
  uint32_t result = *(volatile uint32_t *)((uintptr_t) base + reg);
  barrier_sync();
  return result;
}

static void cmdwait(volatile void *smhc, void *HALsb)
{
  uint32_t ticks = CMD_LOAD_TIMEOUT;
  uint32_t counter = readcounter(HALsb);
  for (;;)
  {
    if ((rreg(smhc, SD_CMD) & CMD_LOAD) == 0)
      break;
    uint32_t new_counter = readcounter(HALsb);
    if (new_counter > counter /* counter wrapped */ && --ticks == 0)
      break; /* timed out */
    counter = new_counter;
  }
}

static void set_vdd_wifi(sdhcidevice_t *dev, uint32_t slot, uint32_t voltage)
{
  void *HALsb = ((controller_t *)dev)->smhc.HALsb;
  IGNORE(slot);

  /* VCC-WIFI is controlled by output DLDO4 of the AXP803 power management IC,
   * which is accessed over the I2C-like RSB bus. The DLDO4 voltage control
   * register (18H) defaults to 3.3V, so we don't need to change that, but
   * the DLDO4 on-off control in register 12H should ideally be toggled here as
   * part of the bus reset sequence. This also serves to turn it on initially,
   * since it defaults to off. */
  uint8_t power_control = callRSBReadByte(0, 0x12, HALsb);
  if (voltage == 0)
    power_control &= ~(1u<<6);
  else
    power_control |= (1u<<6);
  callRSBWriteByte(power_control, 0x12, HALsb);
}

static uint32_t computeclock(uint32_t src, uint32_t desired, uint32_t *log2prediv)
{
  /* Find ideal division ratio, rounding up */
  uint32_t divider = (src + desired - 1) / desired;
  *log2prediv = 0;
  while (divider > 16 && *log2prediv < 3)
  {
    /* Increase predivider at cost of precision in divider */
    /* Ensure we round up when halving divider */
    divider = (divider & 1) + (divider >> 1);
    ++*log2prediv;
  }
  return divider;
}

static uint32_t set_sdclk(sdhcidevice_t *dev, uint32_t slot, uint32_t freq_khz)
{
  volatile void *smhc = dev->dev.address;
  void *HALsb = ((controller_t *)dev)->smhc.HALsb;
  volatile void *ccu = ((controller_t *)dev)->ccu;
  uint32_t clk_reg = ((controller_t *)dev)->clk_reg;
  IGNORE(slot);

  /* The relationship between clocks is as follows:
   *
   * SMHCn_CLK_REG[25:24] selects the clock source. This can be one of
   * * OSC24M, 24 MHz.
   * * PLL_PERIPH0(2X), which in practice is 1.2 GHz. This is related to the
   *   24 MHz clock by factors N (PLL_PERIPH0_CTRL_REG[12:8]+1) and
   *   K (PLL_PERIPH0_CTRL_REG[5:4]+1), but the datasheet recommends that this
   *   is not changed from 1.2 GHz.
   * * PLL_PERIPH1(2X), which is disabled and so should not be used.
   *
   * This is then divided by N (2^SMHCn_CLK_REG[17:16]),
   * M (SMHC0_CLK_REG[3:0]+1), and a fixed factor of 2. There is then a further
   * divider in the SMHC block. Unmentioned in the datasheet is the fact that
   * if SD_CLKDIV[7:0] = 0, then this divider is bypassed; this is clearly a
   * valid mode of operation, because the bootloader leaves it in this state.
   * For any other value, the clock is divided by 2*SD_CLKDIV[7:0].
   *
   * In practice, the two clock sources and range of dividers in SMHCn_CLK_REG
   * are sufficient alone to achieve most of the clock speeds we likely need:
   *    50 kHz = 24 MHz / (2^1) / (11+1) / 2 / 10
   *   400 kHz = 24 MHz / (2^1) / (14+1) / 2 / 1
   *    20 MHz = 1.2 GHz / (2^1) / (15+1) / 2 / 1
   *    25 MHz = 1.2 GHz / (2^1) / (11+1) / 2 / 1
   *    50 MHz = 1.2 GHz / (2^0) / (11+1) / 2 / 1
   *   100 MHz = 1.2 GHz / (2^0) / (5+1) / 2 / 1
   *   200 MHz = 1.2 GHz / (2^0) / (2+1) / 2 / 1
   *
   *   26 MHz and 52 MHz (common speeds for MMC) can't be achieved exactly
   *   using any combination of sources and dividers - the next-lowest are
   *   25 MHz and 50 MHz respectively.
   */
  uint32_t pll_periph0_ctrl_reg = rreg(ccu, PLL_PERIPH0_CTRL_REG);
  uint32_t N = ((pll_periph0_ctrl_reg >> 8) & 0x1F) + 1;
  uint32_t K = ((pll_periph0_ctrl_reg >> 4) & 3) + 1;
  uint32_t src = 24000 * N * K;

  /* First try deriving from PLL_PERIPH0(2X) */
  uint32_t smhcn_clk_reg = SCLK_GATING | CLK_SRC_SEL_PLL_PERIPH02X;
  uint32_t log2prediv;
  uint32_t divider = computeclock(src / 2, freq_khz, &log2prediv);
  uint32_t cclk_div = 1;
  if (divider > 16)
  {
    /* Too slow to divide this way, try 24 MHz source instead */
    src = 24000;
    smhcn_clk_reg = SCLK_GATING | CLK_SRC_SEL_OSC24M;
    divider = computeclock(src / 2, freq_khz, &log2prediv);
    if (divider > 16)
    {
      /* Will need to use CCLK_DIV as well. A divide-by-10 in SMHC yields a
       * useful additional range of round-numbered clock speeds. */
      cclk_div = 10;
      divider = computeclock(src / 2, freq_khz * cclk_div, &log2prediv);
      if (divider > 16)
        divider = 16; // just in case...
    }
  }
  smhcn_clk_reg |= (log2prediv << CLK_DIV_RATIO_N_SHIFT) | ((divider - 1) << CLK_DIV_RATIO_M_SHIFT);

  setreg(smhc, SD_CLKDIV, 0);                // turn off clock
  setreg(smhc, SD_CMD, CMD_LOAD | PROG_CLK); // action the clock change
  cmdwait(smhc, HALsb);                      // check if cmd completed
  setreg(ccu, clk_reg, smhcn_clk_reg);
  if (cclk_div == 1)
    setreg(smhc, SD_CLKDIV, CCLK_ENB);       // turn on clock
  else
    setreg(smhc, SD_CLKDIV, CCLK_ENB | (cclk_div >> 1)); // turn on clock
  setreg(smhc, SD_CMD, CMD_LOAD | PROG_CLK); // action the clock change
  cmdwait(smhc, HALsb);                      // check if cmd completed
  setreg(smhc, SMHC_RINTSTS, -1u);           // reset raw interrupts
  // We need to tell SMHC2 about whether we're using HS400 (i.e. no, not at present)
  if (dev->dev.devicenumber == SMHC2_IRQ)
  {
    setreg(smhc, SMHC_EMMC_DDR_SBIT_DET, rreg(smhc, SMHC_EMMC_DDR_SBIT_DET) &~ HS_MD_EN);
    setreg(smhc, SMHC_CSDC, (rreg(smhc, SMHC_CSDC) &~ CRC_DET_PARA_MASK) | CRC_DET_PARA_OTHER);
  }
  // Sample and data-strobe delay calibration isn't performed by the Linux
  // driver and doesn't appear to produce usable results if we try it. Linux
  // programs fixed delays derived from internal default tables, with overrides
  // from the device tree, with different settings according to the bus speed.
  // For the speeds we currently support, this always amounts to a delay of 0,
  // so we will do likewise.
  if (dev->dev.devicenumber == SMHC2_IRQ)
  {
    setreg(smhc, SMHC_SAMP_DL_REG, SAMP_DL_SW_EN);
    setreg(smhc, SMHC_DS_DL_REG, DS_DL_SW_EN);
  }
  // Sample timing phase on SMHC0 & 1
  // The device tree suggests that - for SMHC1 - we should use 270 degrees in
  // DDR50, and 180 degrees for clock speeds above 52 MHz. Otherwise (or for
  // SMHC0 in all cases) use 90 degrees. Since we don't support those speeds
  // yet, kick the can down the road.
  if (dev->dev.devicenumber != SMHC2_IRQ)
    setreg(smhc, SMHC_NTSR_REG, MODE_SELEC | SAMPLE_TIMING_PHASE_RX_90);
  return (src >> (log2prediv + 1)) / divider / cclk_div;
}

static uint32_t get_tmclk(sdhcidevice_t *dev, uint32_t slot)
{
  volatile void *smhc = dev->dev.address;
  volatile void *ccu = ((controller_t *)dev)->ccu;
  uint32_t clk_reg = ((controller_t *)dev)->clk_reg;
  IGNORE(slot);

  // For this controller, timeouts are measured by the card clock
  uint32_t smhcn_clk_reg = rreg(ccu, clk_reg);
  uint32_t src = 24000;
  if ((smhcn_clk_reg & CLK_SRC_SEL_MASK) == CLK_SRC_SEL_PLL_PERIPH02X)
  {
    uint32_t pll_periph0_ctrl_reg = rreg(ccu, PLL_PERIPH0_CTRL_REG);
    uint32_t N = ((pll_periph0_ctrl_reg >> 8) & 0x1F) + 1;
    uint32_t K = ((pll_periph0_ctrl_reg >> 4) & 3) + 1;
    src *= N * K;
  }
  uint32_t log2prediv = (smhcn_clk_reg & CLK_DIV_RATIO_N_MASK) >> CLK_DIV_RATIO_N_SHIFT;
  uint32_t divider = ((smhcn_clk_reg & CLK_DIV_RATIO_M_MASK) >> CLK_DIV_RATIO_M_SHIFT) + 1;
  uint32_t cclk_div = rreg(smhc, SD_CLKDIV) & CDIV_MASK;
  if (cclk_div == 0)
    cclk_div = 1;
  else
    cclk_div <<= 1;
  return (src >> (log2prediv + 1)) / divider / cclk_div;
}

static bool get_card_detect(sdhcidevice_t *dev, uint32_t slot)
{
  volatile void *pc = ((controller_t *) dev)->pc;
  IGNORE(slot);
  /* SD card slot CD is at port F pin 6, active low */
  return !(rreg(pc, 0x24*5+0x10) & (1u<<6));
}

static void slot_reset(sdhcidevice_t *dev, uint32_t slot)
{
  volatile void *smhc = dev->dev.address;
  volatile void *ccu = ((controller_t *)dev)->ccu;
  uint32_t soft_rst_bit = ((controller_t *)dev)->soft_rst_bit;
  void *HALsb = ((controller_t *)dev)->smhc.HALsb;
  IGNORE(slot);

  // Assert SMHCn reset bit
  setreg(ccu, BUS_SOFT_RST_REG0, rreg(ccu, BUS_SOFT_RST_REG0) &~ (1u<<soft_rst_bit));
  // Do a short delay
  microdelay(10, HALsb);
  // Deassert SMHCn reset bit
  setreg(ccu, BUS_SOFT_RST_REG0, rreg(ccu, BUS_SOFT_RST_REG0) | (1u<<soft_rst_bit));
  // Do longer delay to let it start
  microdelay(100, HALsb);

  // Set the reset bit in SMHC
  setreg(smhc, SD_CTRL, SOFT_RST);
  // Poll for SOFT_RST to clear (in practice this seems to happen immediately)
  for (uint32_t polls = 0x10000; polls > 0 && (rreg(smhc, SD_CTRL) & SOFT_RST); --polls);
  // Enable DMA
  setreg(smhc, SD_CTRL, DMA_ENB);
  // We will shortly be called via set_data_timeout(), which will allow us to
  // set the TIME_UNIT_DAT bit appropriately
}

void device_Init(controller_t mdev[3], volatile void *SMHC0, volatile void *SMHC1, volatile void *SMHC2, volatile void *CCU, volatile void *PC, volatile void *PC2, void (*OS_AddDevice)(uint32_t, struct device *), void *HALsb);
void device_Init(controller_t mdev[3], volatile void *SMHC0, volatile void *SMHC1, volatile void *SMHC2, volatile void *CCU, volatile void *PC, volatile void *PC2, void (*OS_AddDevice)(uint32_t, struct device *), void *HALsb)
{
  for (int i = 0; i < 3; ++i)
  {
    A64_InitSDDeviceStruct(&mdev[i].smhc,
                           (void *) (volatile void *[]){ SMHC0, SMHC1, SMHC2 }[i],
                           HALsb,
                           true,
                           i == 2,
                           i == 0 ? 0x200700F8 : 0x300F00F0); // recommended by user manual
    mdev[i].smhc.dev.dev.devicenumber = (const int32_t []){ SMHC0_IRQ, SMHC1_IRQ, SMHC2_IRQ }[i];
    // The eMMC seems incapable of transfers of more than 2^4 = 16 blocks at
    // any speed or bus width. This limitation doesn't apply to the micro-SD
    // slot, so it's likely a bug in the eMMC chip rather than the controller.
    if (i == 2)
      mdev[i].smhc.dev.flags |= (4 + 1) << HALDeviceSDHCI_Flag_BlockCountLimitShift;
    if (i == 1)
      mdev[i].smhc.dev.SetVdd           = set_vdd_wifi;
    mdev[i].smhc.dev.SetSDCLK           = set_sdclk;
    mdev[i].smhc.dev.GetTMCLK           = get_tmclk;
    if (i == 0)
      mdev[i].smhc.dev.GetCardDetect    = get_card_detect;
    mdev[i].smhc.dev.ver.soft.SlotReset = slot_reset;
    mdev[i].smhc.slot_info.flags        = (const uint32_t []){
                                            HALDeviceSDHCI_SlotFlag_Bus4Bit,
                                            HALDeviceSDHCI_SlotFlag_Integrated | HALDeviceSDHCI_SlotFlag_Bus4Bit,
                                            HALDeviceSDHCI_SlotFlag_Bus8Bit | HALDeviceSDHCI_SlotFlag_IntegratedMem
                                          }[i];
    mdev[i].ccu                         = CCU;
    mdev[i].pc                          = PC;
    mdev[i].pc2                         = PC2;
    mdev[i].clk_reg                     = (const uint8_t []){ SMHC0_CLK_REG, SMHC1_CLK_REG, SMHC2_CLK_REG }[i];
    mdev[i].soft_rst_bit                = (const uint8_t []){ SMHC0_RST_SHIFT, SMHC1_RST_SHIFT, SMHC2_RST_SHIFT }[i];
    mdev[i].port                        = (const uint8_t []){ PORT_SMHC0, PORT_SMHC1, PORT_SMHC2 }[i];
    mdev[i].cmd_pin                     = (const uint8_t []){ PIN_SMHC0_CMD, PIN_SMHC1_CMD, PIN_SMHC2_CMD }[i];
    OS_AddDevice(0, &mdev[i].smhc.dev.dev);
  }
}
