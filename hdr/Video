; Copyright 2009 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     hdr:VideoDevice
	GET     hdr:VIDCList
; Hardware cursor size limits
HW_CURSOR_WIDTH         * 32
HW_CURSOR_HEIGHT        * 32
HW_CURSOR_WIDTH_POW2    * 5
; so min length of a vidc3 list...
VIDCList3_Size                  *       4+VIDCList3_ControlList-VIDCList3_Type

MaxPermittedPixelKHz            *       270000  ; max permitted pixel rate

VIDEO_IRQ               * 127+32 ;25

; DSS registers - relative to L4_Display
DSS_SYSCONFIG           * &010
DSS_SYSSTATUS           * &014
DSS_IRQSTATUS           * &018
DSS_CONTROL             * &040
DSS_SDI_CONTROL         * &044
DSS_PLL_CONTROL         * &048
DSS_SDI_STATUS          * &05C

; DISPC - relative to L4_Display
DISPC_SYSCONFIG                 * &410
DISPC_SYSTATUS                  * &414
DISPC_IRQSTATUS                 * &418
DISPC_IRQENABLE                 * &41C
DISPC_CONTROL                   * &440
DISPC_CONFIG                    * &444
DISPC_DEFAULT_COLOR0            * &44C
DISPC_DEFAULT_COLOR1            * &450
DISPC_TRANS_COLOR0              * &454
DISPC_TRANS_COLOR1              * &458
DISPC_LINE_STATUS               * &45C
DISPC_LINE_NUMBER               * &460
DISPC_TIMING_H                  * &464
DISPC_TIMING_V                  * &468
DISPC_POL_FREQ                  * &46C
DISPC_DIVISOR                   * &470
DISPC_GLOBAL_ALPHA              * &474
DISPC_SIZE_DIG                  * &478
DISPC_SIZE_LCD                  * &47C
DISPC_GFX_BA0                   * &480
DISPC_GFX_BA1                   * &484
DISPC_GFX_POSITION              * &488
DISPC_GFX_SIZE                  * &48C
DISPC_GFX_ATTRIBUTES            * &4A0
DISPC_GFX_FIFO_THRESHOLD        * &4A4
DISPC_GFX_FIFO_SIZE_STATUS      * &4A8
DISPC_GFX_ROW_INC               * &4AC
DISPC_GFX_PIXEL_INC             * &4B0
DISPC_GFX_WINDOW_SKIP           * &4B4
DISPC_GFX_TABLE_BA              * &4B8
DISPC_VID1                      * &4BC
DISPC_VID2                      * &54C
DISPC_VIDn_BA0                  * &000 ; Relative to DISP_VID1/DISPC_VID2
DISPC_VIDn_BA1                  * &004
DISPC_VIDn_POSITION             * &008
DISPC_VIDn_SIZE                 * &00C
DISPC_VIDn_ATTRIBUTES           * &010
DISPC_VIDn_FIFO_THRESHOLD       * &014
DISPC_VIDn_FIFO_SIZE_STATUS     * &018
DISPC_VIDn_ROW_INC              * &01C
DISPC_VIDn_PIXEL_INC            * &020
DISPC_VIDn_FIR                  * &024
DISPC_VIDn_PICTURE_SIZE         * &028
DISPC_VIDn_ACCU0                * &02C
DISPC_VIDn_ACCU1                * &030
DISPC_VIDn_FIR_COEF_H0          * &034
DISPC_VIDn_FIR_COEF_HV0         * &038
DISPC_VIDn_FIR_COEF_H1          * &03C
DISPC_VIDn_FIR_COEF_HV1         * &040
DISPC_VIDn_FIR_COEF_H2          * &044
DISPC_VIDn_FIR_COEF_HV2         * &048
DISPC_VIDn_FIR_COEF_H3          * &04C
DISPC_VIDn_FIR_COEF_HV3         * &050
DISPC_VIDn_CONV_COEF0           * &054
DISPC_VIDn_CONV_COEF1           * &058
DISPC_VIDn_CONV_COEF2           * &05C
DISPC_VIDn_CONV_COEF3           * &060
DISPC_VIDn_CONV_COEF4           * &064
DISPC_DATA_CYCLE0               * &5D4
DISPC_VID0_FIR_COEF_V           * &5E0
DISPC_VID1_FIR_COEF_V           * &600
DISPC_CPR_COEF_R                * &620
DISPC_CPR_COEF_G                * &624
DISPC_CPR_COEF_B                * &628
DISPC_GFX_PRELOAD               * &62C
DISPC_VID0_PRELOAD              * &630
DISPC_VID1_PRELOAD              * &634

; RFBI - Relative to L4_Display
RFBI_SYSCONFIG                  * &810
RFBI_SYSTATUS                   * &814
RFBI_CONTROL                    * &840
RFBI_PIXEL_CNT                  * &844
RFBI_LINE_NUMBER                * &848
RFBI_CMD                        * &84C
RFBI_PARAM                      * &850
RFBI_DATA                       * &854
RFBI_READ                       * &858
RFBI_STATUS                     * &85C
RFBI_CONFIG0                    * &860
RFBI_ONOFF_TIME0                * &864
RFBI_CYCLE_TIME0                * &868
RFBI_DATA_CYCLE1_0              * &86C
RFBI_DATA_CYCLE2_0              * &870
RFBI_DATA_CYCLE3_0              * &874
RFBI_CONFIG1                    * &878
RFBI_ONOFF_TIME1                * &87C
RFBI_CYCLE_TIME1                * &880
RFBI_DATA_CYCLE1_1              * &884
RFBI_DATA_CYCLE2_1              * &888
RFBI_DATA_CYCLE3_1              * &88C
RFBI_VSYNC_WIDTH                * &890
RFBI_HSYNC_WIDTH                * &894

; DSI PLL registers - relative to L4_Display
DSI_PLL_CONTROL                 * &00-256
DSI_PLL_STATUS                  * &04-256
DSI_PLL_GO                      * &08-256
DSI_PLL_CONFIGURATION1          * &0C-256
DSI_PLL_CONFIGURATION2          * &10-256

; DSI registers - relative to L4_Display
DSI_CLK_CTRL                    * &54-&400

; VENC registers - relative to L4_Display
VENC_STATUS                     * &C04
VENC_F_CONTROL                  * &C08
VENC_VIDOUT_CTRL                * &C10
VENC_SYNC_CTRL                  * &C14
VENC_LLEN                       * &C1C
VENC_FLENS                      * &C20
VENC_HFLTR_CTRL                 * &C24
VENC_CC_CARR_WSS_CARR           * &C28
VENC_C_PHASE                    * &C2C
VENC_GAIN_U                     * &C30
VENC_GAIN_V                     * &C34
VENC_GAIN_Y                     * &C38
VENC_BLACK_LEVEL                * &C3C
VENC_BLANK_LEVEL                * &C40
VENC_X_COLOR                    * &C44
VENC_M_CONTROL                  * &C48
VENC_BSTAMP_WSS_DATA            * &C4C
VENC_S_CARR                     * &C50
VENC_LINE21                     * &C54
VENC_LN_SEL                     * &C58
VENC_L21_WC_CTL                 * &C5C
VENC_HTRIGGER_VTRIGGER          * &C60
VENC_SAVID_EAVID                * &C64
VENC_FLEN_FAL                   * &C68
VENC_LAL_PHASE_RESET            * &C6C
VENC_HS_INT_START_STOP_X        * &C70
VENC_HS_EXT_START_STOP_X        * &C74
VENC_VS_INT_START_X             * &C78
VENC_VS_INT_STOP_X_VS_INT_START_Y * &C7C
VENC_VS_INT_STOP_Y_VS_EXT_START_X * &C80
VENC_VS_EXT_STOP_X_VS_EXT_START_Y * &C84
VENC_VS_EXT_STOP_Y              * &C88
VENC_AVID_START_STOP_X          * &C90
VENC_AVID_START_STOP_Y          * &C94
VENC_FID_INT_START_X_FID_INT_START_Y  * &CA0
VENC_FID_INT_OFFSET_Y_FID_EXT_START_X * &CA4
VENC_FID_EXT_START_Y_FID_EXT_OFFSET_Y * &CA8
VENC_TVDETGP_INT_START_STOP_X   * &CB0
VENC_TVDETGP_INT_START_STOP_Y   * &CB4
VENC_GEN_CTRL                   * &CB8
VENC_OUTPUT_CONTROL             * &CC4
VENC_OUTPUT_TEST                * &CC8

; -----------------------------------------------------------------------------------
                        ^ 0
;VDUDevSpec_SizeField    # 4 ; Size field
;VDUDevSpec_Flags        # 4 ; Misc flags
;VDUDevSpec_HDMI_TX_INT  # 4 ; hdmi transmitter interrupt number
;VDUDevSpec_CCM_Base     # 4 ; CCM base address
;VDUDevSpec_IOMUXC_Base  # 4 ; IOMUXC base address
;VDUDevSpec_HDMI_Log     # 4 ; HDMI base address
;VDUDevSpec_SRC_Log      # 4 ; System Reset unit logical address
;VDUDevSpec_VOP_Log     # 4 ;
;VDUDevSpec_IPU2_Log     # 4 ;
;VDUDevSpec_CCMAn_Log    # 4 ;
;VDUDevSpec_Size         # 0 ; Size valu

                        ^ 0
VDUDevSpec_SizeField    # 4 ; Size field
VDUDevSpec_Flags        # 4 ; Misc flags
VDUDevSpec_HDMI_TX_INT  # 4 ; hdmi transmitter interrupt number
VDUDevSpec_CCM_Base     # 4 ; CCM base address
VDUDevSpec_IOMUXC_Base  # 4 ; IOMUXC base address
VDUDevSpec_HDMI_Log     # 4 ; HDMI base address
VDUDevSpec_SRC_Log      # 4 ; System Reset unit logical address
VDUDevSpec_VOP_Log     # 4 ;
VDUDevSpec_IPU2_Log     # 4 ;
VDUDevSpec_CCMAn_Log    # 4 ;
VDUDevSpec_Size         # 0 ; Size value to write to size field

                      ^    0, a1
; Public bits
VideoDeviceDevice     #    HALDevice_VDU_Size
; Private bits
VideoWorkspace        #   4 ; VDUDevSpec_Size ;4 ; HAL workspace pointer
Video_HAL_sb          #   4
Video_Backlight       #   4
Video_HAL_de          #   4
Video_LCD_de          #   4
Video_DMAChan_Log     #   4 ; DMA channel for GraphicsV_Render
Video_DeviceSize      *    :INDEX: @

        END
